import sys
import os
import sqlite3
import MySQLdb
import pprint
import re
import shutil
import hashlib

from datetime import datetime

if len(sys.argv) != 2:
    print "Error: usage: trac2redmine path/to/trac/env"
    sys.exit(1)

abs_env = os.path.abspath(sys.argv[1])
bhdbPath = os.path.join(abs_env, "db", "bloodhound.db")
attachment_path = os.path.join(abs_env, "products")
redmine_attachment_path = os.path.join("C:\Bitnami", "redmine-3.2.2-0", "apps", "redmine", "htdocs", "files");

for the_file in os.listdir(redmine_attachment_path):
    file_path = os.path.join(redmine_attachment_path, the_file)
    try:
        if os.path.isfile(file_path):
            os.unlink(file_path)
        elif os.path.isdir(file_path): shutil.rmtree(file_path)
    except Exception as e:
        print(e)

sqlnow = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

conn = sqlite3.connect(bhdbPath)
c = conn.cursor()

sql = MySQLdb.connect(host="localhost",
                      user="root",
                      passwd="password",
                      db="bitnami_redmine",
                      use_unicode=True,
                      charset="utf8")

cur = sql.cursor()

tables_to_delete = ["projects", "enabled_modules", "projects_trackers", "users", "email_addresses", "issues", "custom_fields", "custom_fields_trackers", "custom_values", "journals", "journal_details", "watchers", "issue_categories", "versions", "attachments"]
for table in tables_to_delete:
    cur.execute(("""DELETE FROM %s""" % (table,)))
    cur.execute(("""ALTER TABLE %s AUTO_INCREMENT=1""" % (table,)))

cur.execute("""INSERT INTO custom_fields(type, name, field_format, possible_values, `regexp`, min_length, max_length, is_required, is_for_all, is_filter, position, searchable, default_value, editable, visible, multiple, format_store, description) VALUES ("IssueCustomField", "Bloodhound ID", "int", NULL, "^[0-9]+$", NULL, NULL, 0, 1, 1, 1, 0, "", 1, 1, 0, "--- !ruby/hash:ActiveSupport::HashWithIndifferentAccess
url_pattern: ''", "The id that this ticket had in bloodhound (may be the same)")""")
cur.execute("""INSERT INTO custom_fields(type, name, field_format, possible_values, `regexp`, min_length, max_length, is_required, is_for_all, is_filter, position, searchable, default_value, editable, visible, multiple, format_store, description) VALUES ("IssueCustomField", "Found in version", "version", NULL, "", NULL, NULL, 0, 1, 1, 2, 0, NULL, 1, 1, 0, "--- !ruby/hash:ActiveSupport::HashWithIndifferentAccess
version_status: []
edit_tag_style: \'\'", "The version this issue was found in")""")
for i in range(1, 4):
    cur.execute("""INSERT INTO custom_fields_trackers(custom_field_id, tracker_id) VALUES(1, %s)""", (i,))
cur.execute("""INSERT INTO custom_fields_trackers(custom_field_id, tracker_id) VALUES(2, 1)""")

c.execute("""SELECT prefix, name, description FROM bloodhound_product""")

project_id = 1
lft = 1

modules = ["issue_tracking", "time_tracking", "news", "documents", "files",
           "wiki", "repository", "boards", "calendar", "gantt"]

issue_categories = ["Software", "Hardware", "Accessory", "Documentation", "General"]

product_dict = {}

print "Migrating projects"
rows = c.fetchall()
for row in rows:
    prefix = row[0]
    name = row[1]
    description = row[2]
    cur.execute("""INSERT INTO projects(id, name, description, is_public, created_on, updated_on, identifier, status, lft, rgt, inherit_members) VALUES (%s, %s, %s, 1, %s, %s, %s, 1, %s, %s, 0)""",
                (project_id, name, description, sqlnow, sqlnow, prefix, lft, lft + 1))
    lft += 2
    for module in modules:
        cur.execute("""INSERT INTO enabled_modules(project_id, name) VALUES(%s, %s)""",
                    (project_id, module))
    for i in range(1, 4):
        cur.execute("""INSERT INTO projects_trackers(project_id, tracker_id) VALUES(%s, %s)""",
                    (project_id, i))
    for cat in issue_categories:
        cur.execute("""INSERT INTO issue_categories(project_id, name, assigned_to_id) VALUES(%s, %s, NULL)""", (project_id, cat))
    product_dict[prefix] = {'id': project_id, 'name': name}
    project_id += 1

print "Projects migrated"
print "Migrating users"

email_map = {}
c.execute("""SELECT sid, value FROM session_attribute WHERE name='name'""")
for row in c.fetchall():
    login = row[0]
    name = row[1]
    split = name.split(" ")
    first_name = split[0]
    last_name = " ".join(split[1:])
    cur.execute("""INSERT INTO users(login, firstname, lastname, salt, hashed_password, language, created_on, updated_on, type, mail_notification, must_change_passwd) VALUES(%s, %s, %s, '', '353e8061f2befecb6818ba0c034c632fb0bcae1b', 'en', %s, %s, 'User', 'all', 1)""", (login, first_name, last_name, sqlnow, sqlnow))
    cur.execute("""SELECT id FROM users WHERE login = %s""", login)
    user_id = cur.fetchone()
    c.execute("""SELECT value FROM session_attribute WHERE name='email' AND sid=?""", (login,))
    email = c.fetchone()
    if email:
        email_map[email[0].lower()] = login
        cur.execute("""INSERT INTO email_addresses(user_id, address, is_default, created_on, updated_on) VALUES(%s, %s, 1, %s, %s)""", (user_id[0], email[0], sqlnow, sqlnow))

cur.execute("""UPDATE users SET must_change_passwd=0, admin=1 where login='TSchicke'""")
print "Users migrated"

print "Migrating Versions"
sys.stdout.flush()

version_discard_map = {
    '72EE2': [
        '11.0',
        '15.0',
        '16.0',
        '17.0'
    ],
    'Group5': [
        '1.0'
    ],
    'GroupG': [
        '891635.0',
        '891636.0'
    ],
    'Jockey': [
        '1.0'
    ],
    'LoadBankController': [
        '1.0',
        '2.0'
    ],
    'LoadBankHandheld': [
        '1.0',
        '2.0'
    ],
    'MCIO': [
        '1.0',
        'MCIO__X75'
    ],
    'Mark3': [
        '1.0',
        '2.0'
    ],
    'MarkIIXg': [
        '1.0'
    ],
    'Misc_Closed': [
        '1.0',
        '3.0',
        '11.0'
    ],
    'PM': [
        '1.0'
    ],
    'Process': [
        '1.0'
    ],
    'Select': [
        '2.0'
    ],
    'TDI': [
        
    ]
}
version_clean_map = {}
version_delete_map = {}
version_array = {}

skip_regex = re.compile(ur'72EE(.*)')
discard_regex = re.compile(ur'milestone', re.IGNORECASE)
accept_regex = re.compile(ur'sprint|phase', re.IGNORECASE)
version_regex = re.compile(ur'(?:[0-9]+-)?(x?[0-9]+(?:\.[0-9]+)?[a-zA-Z]?).*', re.IGNORECASE)
commit_regex = re.compile(ur'(x[0-9]+)', re.IGNORECASE)
zero_regex = re.compile(ur'^0+\.0+')
dot_regex = re.compile(ur'x|\.', re.IGNORECASE)
def get_clean_version(version):
    if version is None:
        return None
    clean_version = version
    if discard_regex.search(clean_version):
        return None
    if accept_regex.search(clean_version):
        return clean_version
    skip_match = skip_regex.search(clean_version)
    if skip_match:
        clean_version = skip_match.group(1)
    match = version_regex.search(clean_version)
    if match == None:
        return version
    clean_version = match.group(1)
    if clean_version == None:
        return None
    if zero_regex.search(clean_version):
        return None
    if not dot_regex.search(clean_version):
        clean_version = clean_version + '.0'
    if commit_regex.search(clean_version):
        clean_version = clean_version.lstrip('0')
    else:
        clean_version = clean_version.strip('0')
    if clean_version[0] == '.':
        clean_version = '0' + clean_version
    if clean_version[len(clean_version) - 1] == '.':
        clean_version = clean_version + '0'
    return clean_version

def map_version(version, product, due):
    clean_version = get_clean_version(version)
    if clean_version:
        if product in version_discard_map and clean_version in version_discard_map[product]:
            return
        if product not in version_clean_map:
            version_clean_map[product] = {}
        if product not in version_array:
            version_array[product] = []
        version_clean_map[product][version] = clean_version
        if (clean_version, due) not in version_array[product]:
            version_array[product].append((clean_version, due))
    elif version is not None and version != "":
        if product not in version_delete_map:
            version_delete_map[product] = []
        version_delete_map[product].append(version) 

c.execute("""SELECT name, due, completed, description, product FROM milestone""")
for row in c.fetchall():
    name = row[0]
    due = row[1]
    due_date = datetime.fromtimestamp(due / 1000000).strftime("%Y-%m-%d")
    completed = row[2]
    description = row[3]
    product = row[4]

    map_version(name, product, due_date)

c.execute("""SELECT DISTINCT version, product FROM ticket""")
for row in c.fetchall():
    name = row[0]
    product = row[1]

    map_version(name, product, None)

c.execute("""SELECT DISTINCT milestone, product FROM ticket""")
for row in c.fetchall():
    name = row[0]
    product = row[1]

    map_version(name, product, None)

print "Version Array:"
for product, ver_array in version_array.iteritems():
    if product not in product_dict:
        continue
    product_id = product_dict[product]['id']
    print '%s - %d' % (product, product_id)
    for ver in ver_array:
        print '\t%s' % (str(ver),)
        cur.execute("""INSERT INTO versions(project_id, name, description, effective_date, created_on, updated_on, wiki_page_title, status, sharing) VALUES(%s, %s, '', %s, %s, %s, '', 'open', 'none')""", (product_id, ver[0], ver[1], sqlnow, sqlnow))
    
print "Versions Migrated"
sys.stdout.flush()

print '\n\n'
print "Cleaned versions:"
for product, ver_map in version_clean_map.iteritems():
    print '%s:' % (product,)
    for old_ver, new_ver in ver_map.iteritems():
        print '\t%s --> %s' % (old_ver, new_ver)

print '\n'
print "Deleted versions:"
for product, ver_array in version_delete_map.iteritems():
    print '%s:' % (product,)
    for del_ver in ver_array:
        print '\t%s --> deleted' % (del_ver)
        
print '\n\n'
print "Building version map"

version_map = {}
cur.execute("""SELECT id, project_id, name FROM versions""")
for row in cur.fetchall():
    version_id = row[0]
    project_id = row[1]
    name = row[2]
    if project_id not in version_map:
        version_map[project_id] = {}
    version_map[project_id][name] = version_id

pprint.pprint(version_map)
    
cur.execute("""SELECT name, id FROM trackers""")

print '\n\n'
print "Building tracker map"
tracker_map = {}
for row in cur.fetchall():
    tracker_map[row[0]] = int(row[1])

pprint.pprint(tracker_map)

bh_type_map = {
    'Defect': 'Bug',
    'defect': 'Bug',
    'enhancement': 'Feature',
    'task': 'Support',
    'Improve': 'Support',
    'Doc.': 'Support',
    'User Experience': 'Support',
    'Weakness': 'Support',
    'Requirement (New)': 'Feature',
    'New Feature': 'Feature',
    'New Task': 'Feature',
    'Business Rules': 'Support',
    'Minutes': 'Support',
    'Not An Issue': 'Support',
    'Needs Review': 'Support',
    'Verify': 'Support'
}

print "Building status map"

cur.execute("""SELECT name, id FROM issue_statuses""")
status_map = {}
for row in cur.fetchall():
    status_map[row[0]] = int(row[1])

pprint.pprint(status_map)

bh_resolution_map = {
    'invalid': 'Rejected',
    'wontfix': 'Rejected',
    'duplicate': 'Closed',
    'fixed': 'Resolved',
    'workaround': 'Resolved',
    'Ignored': 'Rejected',
    'testing': 'Feedback',
    'closed': 'Closed',
    'on hold': 'Closed',
    'future': 'Closed',
    'Open': 'In Progress',
    'Reopen': 'In Progress',
    'Retesting': 'Feedback' 
}

bh_status_map = {
    'reopened': 'In Progress',
    'new': 'New',
    'closed': 'Closed',
    'assigned': 'In Progress'
}

cur.execute("""SELECT name, id FROM enumerations WHERE type='IssuePriority'""")

print "Building priority map"
priority_map = {}
for row in cur.fetchall():
    priority_map[row[0]] = int(row[1])

pprint.pprint(priority_map)

bh_priority_map = {
    'CustomerWaiting': 'Immediate',
    'Urgent': 'Urgent',
    'High': 'High',
    'Medium': 'Normal',
    'Low': 'Low',
    'Needs Feedback': 'Normal',
    'major': 'High',
    'critical': 'Urgent',
}

#TODO migrate old categories?
print "Building category map"
category_map = {}

cur.execute("""SELECT id, project_id, name FROM issue_categories""")
for row in cur.fetchall():
    category_id = row[0]
    project_id = row[1]
    name = row[2]
    if project_id not in category_map:
        category_map[project_id] = {}
    category_map[project_id][name] = category_id

pprint.pprint(category_map)
    
print "Building user map"

cur.execute("""SELECT login, id FROM users""")
user_map = {}
for row in cur.fetchall():
    user_map[row[0]] = int(row[1])
    
pprint.pprint(user_map)

c.execute("""SELECT uid, type, time, changetime, product, summary, description, status, resolution, priority, reporter, id, cc, component, owner, version, milestone FROM ticket""")

email_regex = re.compile(r"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$")
print "Migrating tickets"
ticket_map = {}
for row in c.fetchall():
    ticket_id = row[0]
    bloodhound_id = row[11]
    time = datetime.fromtimestamp(int(row[2]) / 1000000).strftime("%Y-%m-%d %H:%M:%S")
    start_date = datetime.fromtimestamp(int(row[2]) / 1000000).strftime("%Y-%m-%d")
    changetime = datetime.fromtimestamp(int(row[3]) / 1000000).strftime("%Y-%m-%d %H:%M:%S")
    tracker_id = tracker_map[bh_type_map[row[1]]]
    status_id = status_map[bh_resolution_map[row[8]] if (row[8] in bh_resolution_map) else bh_status_map[row[7]]]
    priority_id = priority_map[bh_priority_map[row[9]]] if (row[9] in bh_priority_map) else priority_map["Normal"]
    product = row[4]
    product_id = product_dict[product]['id'] if (product in product_dict) else -1
    author_id = user_map[row[10]] if row[10] in user_map else 1
    assigned_to_id = user_map[row[14]] if row[14] in user_map else None
    subject = row[5]
    description = row[6]
    cc = row[12]
    version = row[15]
    found_in_version_id = None
    milestone = row[16]
    target_version_id = None
    if product in version_clean_map:
        if version in version_clean_map[product]:
            clean_found_in_version = version_clean_map[product][version]
            if product_id in version_map and clean_found_in_version in version_map[product_id]:
                found_in_version_id = version_map[product_id][clean_found_in_version]
        if milestone in version_clean_map[product]:
            clean_target_version = version_clean_map[product][milestone]
            if product_id in version_map and clean_target_version in version_map[product_id]:
                target_version_id = version_map[product_id][clean_target_version]
    if product_id > 0 and priority_id != "":
        if cc is not None:
            addresses = cc.split(u",")
            for address in addresses:
                address = address.strip()
                if email_regex.match(address) and address.lower() in email_map and email_map[address.lower()] in user_map:
                    cur.execute("""INSERT INTO watchers(watchable_type, watchable_id, user_id) VALUES('Issue', %s, %s)""", (ticket_id, user_map[email_map[address.lower()]]))
                elif address in user_map:
                    cur.execute("""INSERT INTO watchers(watchable_type, watchable_id, user_id) VALUES('Issue', %s, %s)""", (ticket_id, user_map[address]))
                else:
                    pass
            
        ticket_map[product + str(row[11])] = ticket_id
        
        category_id = None
        component = row[13]
        if component and product_id in category_map and component in category_map[product_id]:
            category_id = category_map[product_id][component]
        cur.execute("""INSERT INTO issues(id, tracker_id, project_id, subject, description, status_id, priority_id, author_id, lock_version, created_on, updated_on, start_date, done_ratio, root_id, lft, rgt, is_private, category_id, assigned_to_id, fixed_version_id) VALUES(%s, %s, %s, %s, %s, %s, %s, %s, 0, %s, %s, %s, 0, %s, 1, 2, 0, %s, %s, %s)""", (ticket_id, tracker_id, product_id, subject, description, status_id, priority_id, author_id, time, changetime, start_date, ticket_id, category_id, assigned_to_id, target_version_id))
        cur.execute("""INSERT INTO custom_values(customized_type, customized_id, custom_field_id, value) VALUES ("Issue", %s, 1, %s)""", (ticket_id, bloodhound_id))
        if found_in_version_id is not None:
            cur.execute("""INSERT INTO custom_values(customized_type, customized_id, custom_field_id, value) VALUES("Issue", %s, 2, %s)""", (ticket_id, found_in_version_id))

print "Tickets Migrated"

field_map = {
    'status':'status_id',
    'owner':'assigned_to_id',
    'component':'',
    'product':'project_id',
    'version':'fixed_version_id',
}

print "Migrating ticket changes"

c.execute("""SELECT ticket, time, author, field, oldvalue, newvalue, product FROM ticket_change""")
journal_map = {}
for row in c.fetchall():
    time = row[1] / 1000000
    if not time in journal_map:
        journal_map[time] = {}
    author_id = user_map[row[2]] if row[2] in user_map else None
    if author_id is None:
        continue
    if not author_id in journal_map[time]:
        journal_map[time][author_id] = {}
    ticket = row[0]
    product = row[6]
    key = product + str(ticket)
    uid = ticket_map[key] if key in ticket_map else -1
    if uid == -1:
        continue
    if not uid in journal_map[time][author_id]:
        journal_map[time][author_id][uid] = []
    journal_map[time][author_id][uid].append(row)

changes_map = {
    'comment':'comment',
    'summary':'subject',
    'description':'description',
    'status':'status_id',
    'owner':'assigned_to_id',
    'priority':'priority_id',
    'component':'category_id',
    'milestone':'fixed_version_id',
    'version':'2'
}
comment_count = 0
summary_count = 0
description_count = 0
status_count = 0
owner_count = 0
priority_count = 0
for time, author_array in journal_map.iteritems():
    for author_id, uid_array in author_array.iteritems():
        for uid, rows in uid_array.iteritems():
            formatted_time = datetime.fromtimestamp(float(time)).strftime("%Y-%m-%d %H:%M:%S")
            cur.execute("""INSERT INTO journals(journalized_id, journalized_type, user_id, notes, created_on, private_notes) VALUES(%s, 'Issue', %s, %s, %s, 0)""", (uid, author_id, '', formatted_time))
            cur.execute("""SELECT id FROM journals WHERE journalized_id = %s AND user_id = %s AND created_on = %s""", (uid, author_id, formatted_time))
            journal_id = cur.fetchone()[0]
            for i, row in enumerate(rows):
                ticket = row[0]
                #time = datetime.fromtimestamp(int(row[1]) / 1000000).strftime("%Y-%m-%d %H:%M:%S")
                author_id = user_map[row[2]] if row[2] in user_map else None
                field = row[3]
                oldvalue = row[4]
                newvalue = row[5]
                product = row[6]
                product_id = product_dict[product]['id'] if product in product_dict else None
                key = product + str(ticket)
                uid = ticket_map[key] if key in ticket_map else -1

                if uid >= 0 and author_id is not None and product in product_dict and field in changes_map:
                    if field == "comment" and newvalue != '':
                        cur.execute("""UPDATE journals SET notes=%s WHERE journalized_id=%s AND created_on=%s AND user_id=%s""", (newvalue, uid, formatted_time, author_id))
                        comment_count += 1
                    elif field == "summary" or field == "description":
                        if field == "summary":
                            summary_count += 1
                        else:
                            description_count += 1
                        cur.execute("""INSERT INTO journal_details(journal_id, property, prop_key, old_value, value) VALUES(%s, "attr", %s, %s, %s)""", (journal_id, changes_map[field], oldvalue, newvalue))
                    elif field == "status":
                        old_status_id = status_map[bh_status_map[oldvalue]] if (oldvalue in bh_status_map) else None
                        new_status_id = status_map[bh_status_map[newvalue]] if (newvalue in bh_status_map) else None
                        if old_status_id == new_status_id:
                            continue
                        status_count += 1
                        cur.execute("""INSERT INTO journal_details(journal_id, property, prop_key, old_value, value) VALUES(%s, "attr", %s, %s, %s)""", (journal_id, changes_map[field], old_status_id, new_status_id))
                    elif field == "owner":
                        old_author_id = user_map[oldvalue] if oldvalue in user_map else None
                        new_author_id = user_map[newvalue] if newvalue in user_map else None
                        if old_author_id == new_author_id:
                            continue
                        owner_count += 1
                        cur.execute("""INSERT INTO journal_details(journal_id, property, prop_key, old_value, value) VALUES(%s, "attr", %s, %s, %s)""", (journal_id, changes_map[field], old_author_id, new_author_id))
                    elif field == "priority":
                        old_priority_id = priority_map[bh_priority_map[oldvalue]] if (oldvalue in bh_priority_map) else None
                        new_priority_id = priority_map[bh_priority_map[newvalue]] if (newvalue in bh_priority_map) else None
                        if old_priority_id == new_priority_id:
                            continue
                        priority_count += 1
                        cur.execute("""INSERT INTO journal_details(journal_id, property, prop_key, old_value, value) VALUES(%s, "attr", %s, %s, %s)""", (journal_id, changes_map[field], old_priority_id, new_priority_id))
                    elif field == 'component':
                        old_category_id = category_map[product_id][oldvalue] if product_id in category_map and oldvalue in category_map[product_id] else None
                        new_category_id = category_map[product_id][newvalue] if product_id in category_map and newvalue in category_map[product_id] else None
                        if old_category_id == new_category_id:
                            continue                        
                        cur.execute("""INSERT INTO journal_details(journal_id, property, prop_key, old_value, value) VALUES(%s, "attr", %s, %s, %s)""", (journal_id, changes_map[field], old_category_id, new_category_id))
                    elif field == 'version' or field == 'milestone':
                        print "test " + str(journal_id) + "    " + field
                        if product not in version_clean_map or product_id not in version_map:
                            continue
                        old_clean_version = version_clean_map[product][oldvalue] if oldvalue in version_clean_map[product] else None
                        new_clean_version = version_clean_map[product][newvalue] if newvalue in version_clean_map[product] else None
                        print oldvalue, old_clean_version
                        print newvalue, new_clean_version
                        old_found_in_version_id = version_map[product_id][old_clean_version] if old_clean_version in version_map[product_id] else None
                        new_found_in_version_id = version_map[product_id][new_clean_version] if new_clean_version in version_map[product_id] else None
                        print old_found_in_version_id
                        print new_found_in_version_id
                        if old_found_in_version_id == new_found_in_version_id:
                            continue
                        cur.execute("""INSERT INTO journal_details(journal_id, property, prop_key, old_value, value) VALUES(%s, %s, %s, %s, %s)""", (journal_id, "attr" if field == "milestone" else "cf", changes_map[field], old_found_in_version_id, new_found_in_version_id))
                    else:
                        pass
                        

print "Ticket changes migrated:"
print "\t# Comments Migrated: ", comment_count
print "\t# Description Changes Migrated: ", description_count
print "\t# Summary Changes Migrated: ", summary_count
print "\t# Status Changes Migrated: ", status_count
print "\t# Owner Changes Migrated: ", owner_count
print "\t# Priority Changes Migrated: ", priority_count

print "Migrating attachments"
sys.stdout.flush()

mime_map = {
    'text/plain' : ['txt', 'tpl', 'properties', 'patch', 'diff', 'ini', 'readme', 'install', 'upgrade'],
    'text/css' : ['css'],
    'text/html' : ['html', 'htm', 'xhtml'],
    'text/jsp' : ['jsp'],
    'text/x-c' : ['c', 'cpp', 'cc', 'h,hh'],
    'text/x-csharp' : ['cs'],
    'text/x-java' : ['java'],
    'text/x-javascript' : ['js'],
    'text/x-html-template' : ['rhtml'],
    'text/x-perl' : ['pl', 'pm'],
    'text/x-php' : ['php', 'php3', 'php4', 'php5'],
    'text/x-python' : ['py'],
    'text/x-ruby' : ['rb', 'rbw', 'ruby', 'rake', 'erb'],
    'text/x-csh' : ['csh'],
    'text/x-sh' : ['sh'],
    'text/xml' : ['xml', 'xsd', 'mxml'],
    'text/yaml' : ['yml', 'yaml'],
    'text/csv' : ['csv'],
    'text/x-po' : ['po'],
    'image/gif' : ['gif'],
    'image/jpeg' : ['jpg', 'jpeg', 'jpe'],
    'image/png' : ['png'],
    'image/tiff' : ['tiff', 'tif'],
    'image/x-ms-bmp' : ['bmp'],
    'image/x-xpixmap' : ['xpm'],
    'application/pdf' : ['pdf'],
    'application/rtf' : ['rtf'],
    'application/msword' : ['doc'],
    'application/vnd.ms-excel' : ['xls'],
    'application/vnd.ms-powerpoint' : ['ppt', 'pps'],
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document' : ['docx'],
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' : ['xlsx'],
    'application/vnd.openxmlformats-officedocument.presentationml.presentation' : ['pptx'],
    'application/vnd.openxmlformats-officedocument.presentationml.slideshow' : ['ppsx'],
    'application/vnd.oasis.opendocument.spreadsheet' : ['ods'],
    'application/vnd.oasis.opendocument.text' : ['odt'],
    'application/vnd.oasis.opendocument.presentation' : ['odp'],
    'application/x-7z-compressed' : ['7z'],
    'application/x-rar-compressed' : ['rar'],
    'application/x-tar' : ['tar'],
    'application/zip' : ['zip'],
    'application/x-gzip' : ['gz'],
}

def hashfile(afile, hasher, blocksize=65536):
    buf = afile.read(blocksize)
    while len(buf) > 0:
        hasher.update(buf)
        buf = afile.read(blocksize)
    return hasher.hexdigest()

c.execute("""SELECT type, id, filename, size, time, description, author, ipnr, product FROM attachment""")

rows = c.fetchall()
num_rows = len(rows)
for i, row in enumerate(rows):
    print '%d / %d: %d%%' % (i + 1, num_rows, int(i * 100.0 / num_rows))
    sys.stdout.flush()
    ticket = row[1]
    product = row[8]
    key = product + str(ticket)
    uid = ticket_map[key] if key in ticket_map else -1
    if uid == -1:
        continue
    author_id = user_map[row[6]] if row[6] in user_map else None
    if author_id == None:
        continue
    folder_name = hashlib.sha1(row[1].encode('utf-8')).hexdigest()
    base_folder_name = folder_name[:3]
    extension = os.path.splitext(row[2])[1]
    file_name = hashlib.sha1(row[2].encode('utf-8')).hexdigest() + extension
    mime_type = 'text/plain'
    attachment_type = row[0]
    ext = extension[1:]
    for mime_t, extensions in mime_map.iteritems():
        if ext.lower() in extensions:
            mime_type = mime_t
            break
    full_path = os.path.join(attachment_path, product, "files", "attachments", attachment_type, base_folder_name, folder_name, file_name)
    if not os.path.isfile(full_path):
        continue
    if attachment_type == 'wiki':
        continue
    time = datetime.fromtimestamp(int(row[4]) / 1000000).strftime("%Y-%m-%d %H:%M:%S")
    year = str(datetime.fromtimestamp(int(row[4]) / 1000000).year)
    month = datetime.fromtimestamp(int(row[4]) / 1000000).month
    if month < 10:
        month = "0" + str(month)
    else:
        month = str(month)
    make_path = os.path.join(redmine_attachment_path, year)
    if not os.path.exists(make_path):
        os.makedirs(make_path)
    make_path = os.path.join(make_path, month)
    if not os.path.exists(make_path):
        os.makedirs(make_path)

    redmine_file_name = datetime.fromtimestamp(int(row[4] / 1000000)).strftime("%y%m%d%H%M%S") + "_" + row[2]
    dest_path = os.path.join(make_path, redmine_file_name)
    shutil.copy2(full_path, dest_path)
    digest = hashfile(open(dest_path, 'rb'), hashlib.md5())
    
    cur.execute("""INSERT INTO attachments(container_id, container_type, filename, disk_filename, filesize, content_type, digest, downloads, author_id, created_on, description, disk_directory) VALUES(%s, 'Issue', %s, %s, %s, %s, %s, 0, %s, %s, %s, %s)""", (uid, row[2], redmine_file_name, row[3], mime_type, digest, author_id, time, row[5], '/'.join([year, month])))

print "Attachments migrated"
sys.stderr.write("Done")
    
c.close()
conn.close()

sql.commit()
cur.close()
sql.close()
